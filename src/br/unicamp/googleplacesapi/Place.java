package br.unicamp.googleplacesapi;

import java.util.List;

public class Place {
    private String name, id, reference, vicinity, icon;
    private double latitude, longitude;
    private List<GooglePlacesApi.Type> types;

    Place(String name, String id, String reference, String vicinity,
          double latitude, double longitude, String icon, List<GooglePlacesApi.Type> types) {

        this.name = name;
        this.id = id;
        this.reference = reference;
        this.vicinity = vicinity;
        this.latitude = latitude;
        this.longitude = longitude;
        this.icon = icon;
        this.types = types;
    }

    public String getIcon() {
        return icon;
    }

    public String getVicinity() {
        return vicinity;
    }

    public String getReference() {
        return reference;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public List<GooglePlacesApi.Type> getTypes() {
        return types;
    }
}
