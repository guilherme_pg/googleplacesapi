package br.unicamp.googleplacesapi;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class GooglePlacesApi {
    public static final String ENCODING = "UTF-8";
    private static final int MAX_RADIUS = 50000;

    public static final String API_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/search/json?";
    public static final String API_DETAILS_URL = "https://maps.googleapis.com/maps/api/place/details/json?";

    private static final String LOG_TAG = "GoogleMapsApi";

    public enum RankBy {
        PROMINENCE,
        DISTANCE;

        public String toString() {
            return super.toString().toLowerCase();
        }
    };

    public enum Language {
        AR, BG, BN, CA, CS, DA, DE, EL, EN, EN_AU, EN_GB, ES, EU, FA, FI, FIL, FR, GL, GU,
        HI, HR, HU, ID, IT, IW, JA, KN, KO, LT, LV, ML, MR, NL, NN, NO, OR, PL, PT, PT_BR, PT_PT,
        RM, RO, RU, SK, SL, SR, SV, TL, TA, TE, TH, TR, UK, VI, ZH_CN, ZH_TW;

        public String toString() {
            return super.toString().toLowerCase().replace('_', '-');
        }
    }

    public enum Type {
        /* Searches and additions */
        ACCOUNTING, AIRPORT, AMUSEMENT_PARK, AQUARIUM, ART_GALLERY, ATM, BAKERY, BANK, BAR,
        BEAUTY_SALON, BICYCLE_STORE, BOOK_STORE, BOWLING_ALLEY, BUS_STATION, CAFE, CAMPGROUND,
        CAR_DEALER, CAR_RENTAL, CAR_REPAIR, CAR_WASH, CASINO, CEMETERY, CHURCH, CITY_HALL,
        CLOTHING_STORE, CONVENIENCE_STORE, COURTHOUSE, DENTIST, DEPARTMENT_STORE, DOCTOR,
        ELECTRICIAN, ELECTRONICS_STORE, EMBASSY, ESTABLISHMENT, FINANCE, FIRE_STATION,
        FLORIST, FOOD, FUNERAL_HOME, FURNITURE_STORE, GAS_STATION, GENERAL_CONTRACTOR, GEOCODE,
        GROCERY_OR_SUPERMARKET, GYM, HAIR_CARE, HARDWARE_STORE, HEALTH, HINDU_TEMPLE,
        HOME_GOODS_STORE, HOSPITAL, INSURANCE_AGENCY, JEWELRY_STORE, LAUNDRY, LAWYER, LIBRARY,
        LIQUOR_STORE, LOCAL_GOVERNMENT_OFFICE, LOCKSMITH, LODGING, MEAL_DELIVERY, MEAL_TAKEAWAY,
        MOSQUE, MOVIE_RENTAL, MOVIE_THEATER, MOVING_COMPANY, MUSEUM, NIGHT_CLUB, PAINTER, PARK,
        PARKING, PET_STORE, PHARMACY, PHYSIOTHERAPIST, PLACE_OF_WORSHIP, PLUMBER, POLICE,
        POST_OFFICE, REAL_ESTATE_AGENCY, RESTAURANT, ROOFING_CONTRACTOR, RV_PARK, SCHOOL,
        SHOE_STORE, SHOPPING_MALL, SPA, STADIUM, STORAGE, STORE, SUBWAY_STATION, SYNAGOGUE,
        TAXI_STAND, TRAIN_STATION, TRAVEL_AGENCY, UNIVERSITY, VETERINARY_CARE, ZOO,

        /* Only for searches */
        ADMINISTRATIVE_AREA_LEVEL_1, ADMINISTRATIVE_AREA_LEVEL_2,
        ADMINISTRATIVE_AREA_LEVEL_3, COLLOQUIAL_AREA, COUNTRY, FLOOR, INTERSECTION, LOCALITY,
        NATURAL_FEATURE, NEIGHBORHOOD, POLITICAL, POINT_OF_INTEREST, POST_BOX, POSTAL_CODE,
        POSTAL_CODE_PREFIX, POSTAL_TOWN, PREMISE, ROOM, ROUTE, STREET_ADDRESS, STREET_NUMBER,
        SUBLOCALITY, SUBLOCALITY_LEVEL_4, SUBLOCALITY_LEVEL_5, SUBLOCALITY_LEVEL_3,
        SUBLOCALITY_LEVEL_2, SUBLOCALITY_LEVEL_1, SUBPREMISE, TRANSIT_STATION;

        public String toString() {
            return super.toString().toLowerCase();
        }

        public static Type fromString(String s) {
            return valueOf(s.toUpperCase());
        }
    }

    private String key;
    private boolean sensor;
    private DefaultHttpClient http;

    public GooglePlacesApi(String key, boolean sensor) {
        this.key = key;
        this.sensor = sensor;

        http = new DefaultHttpClient();
    }

    public SearchResponse search(SearchQuery sq) throws InvalidSearchException,
                                                        InvalidResponseException,
                                                        IOException {
        String queryString = buildSearchUrl(sq);
        Log.d(LOG_TAG, "Search query string: " + queryString);

        try {
            return new SearchResponse(doGetJSON(queryString));
        } catch (JSONException e) {
            throw new InvalidResponseException("failed to parse response");
        }
    }

    private String buildSearchUrl(SearchQuery sq) throws InvalidSearchException {
        List<String> parameters = new ArrayList<String>();
        String queryString = API_SEARCH_URL + "key=" + key + "&" + "sensor=" + sensor;
        String locationString = String.format(new Locale("en_US"),
                                              "location=%f,%f",
                                              sq.getLatitude(),
                                              sq.getLongitude());

        parameters.add(locationString);
        if (sq.getLanguage() != null) parameters.add("language=" + sq.getLanguage().toString());

        /*
         * as per API documentation, radius should not be specified if
         * rankby=distance, and in that case, at least one of keyword, name and types is required.
         */
        if (sq.getRankBy() != GooglePlacesApi.RankBy.DISTANCE) {
            if (sq.getRadius() > MAX_RADIUS)
                throw new InvalidSearchException("radius must be <= " + Integer.toString(MAX_RADIUS));
            parameters.add("radius=" + sq.getRadius());
        } else if (!(sq.getKeyword() != null || sq.getName() != null || sq.getTypes() != null)) {
            throw new InvalidSearchException("need at least one of keyword, name or types if rankBy=distance");
        }

        try {
            if (sq.getKeyword() != null)
                parameters.add("keyword=" + URLEncoder.encode(sq.getKeyword(), ENCODING));
            if (sq.getName() != null)
                parameters.add("name=" + URLEncoder.encode(sq.getName(), ENCODING));
        } catch (UnsupportedEncodingException e) {
            throw new InvalidSearchException("can't encode search parameters with " + ENCODING);
        }

        if (sq.getTypes() != null && sq.getTypes().size() != 0) {
            String typeString = "types=";
            for (GooglePlacesApi.Type t : sq.getTypes()) {
                if (typeString != "types=") typeString += "|";
                typeString += t.toString();
            }

            parameters.add(typeString);
        }

        for (String p : parameters) {
            queryString += ("&" + p);
        }

        return queryString;
    }

    private JSONObject doGetJSON(String url) throws JSONException, IOException {
        return new JSONObject(doGet(url));
    }

    private String doGet(String url) throws IOException {
        HttpEntity entity;
        HttpResponse response;
        HttpGet request = new HttpGet(url);

        response = http.execute(request);
        entity = response.getEntity();

        if (entity != null) {
            return EntityUtils.toString(entity);
        }

        return null;
    }
}
