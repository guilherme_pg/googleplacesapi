package br.unicamp.googleplacesapi;

import java.util.Arrays;
import java.util.List;

public class SearchQuery {
    private String keyword, name;
    private double latitude, longitude;
    private int radius;
    private GooglePlacesApi.RankBy rankBy;
    private GooglePlacesApi.Language language;
    private List<GooglePlacesApi.Type> types;

    public SearchQuery(double latitude, double longitude, int radius) {
        setLocation(latitude, longitude);
        setRadius(radius);
    }

    /* Mandatory search parameters */
    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public SearchQuery setLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        return this;
    }

    public int getRadius() {
        return radius;
    }

    public SearchQuery setRadius(int radius) {
        this.radius = radius;
        return this;
    }

    /* Optional search parameters */
    public String getKeyword() {
        return keyword;
    }

    public SearchQuery setKeyword(String keyword) {
        this.keyword = keyword;
        return this;
    }

    public String getName() {
        return name;
    }

    public SearchQuery setName(String name) {
        this.name = name;
        return this;
    }

    public GooglePlacesApi.RankBy getRankBy() {
        return rankBy;
    }

    public SearchQuery setRankBy(GooglePlacesApi.RankBy rankBy) {
        this.rankBy = rankBy;
        return this;
    }

    public GooglePlacesApi.Language getLanguage() {
        return language;
    }

    public SearchQuery setLanguage(GooglePlacesApi.Language language) {
        this.language = language;
        return this;
    }

    public List<GooglePlacesApi.Type> getTypes() {
        return types;
    }

    public SearchQuery setTypes(GooglePlacesApi.Type...types) {
        setTypes(Arrays.asList(types));
        return this;
    }

    public SearchQuery setTypes(List<GooglePlacesApi.Type> types) {
        this.types = types;
        return this;
    }
}
