package br.unicamp.googleplacesapi;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;
import java.util.ArrayList;

public class SearchResponse extends Response {
    private List<Place> places;

    SearchResponse(JSONObject rawResponse) throws JSONException {
        super(rawResponse);
        parsePlaces(rawResponse.getJSONArray("results"));
    }

    public List<Place> getPlaces() {
        return places;
    }

    private void parsePlaces(JSONArray rawPlaces) throws JSONException {
        places = new ArrayList<Place>();

        for (int i = 0; i < rawPlaces.length(); i++) {
            JSONObject o = rawPlaces.getJSONObject(i);
            JSONArray rawTypes = o.getJSONArray("types");
            List<GooglePlacesApi.Type> types = new ArrayList<GooglePlacesApi.Type>();

            for (int j = 0; j < rawTypes.length(); j++) {
                types.add(GooglePlacesApi.Type.fromString(rawTypes.getString(j)));
            }

            places.add(
                new Place(o.getString("name"),
                          o.getString("id"),
                          o.getString("reference"),
                          o.getString("vicinity"),
                          o.getJSONObject("geometry").getJSONObject("location").getDouble("lat"),
                          o.getJSONObject("geometry").getJSONObject("location").getDouble("lng"),
                          o.getString("icon"),
                          types));
        }
    }
}
