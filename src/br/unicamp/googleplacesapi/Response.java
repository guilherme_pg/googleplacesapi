package br.unicamp.googleplacesapi;

import org.json.JSONObject;
import org.json.JSONException;

public class Response {
    private Status status;
    private String htmlAttributions;

    public enum Status {
        OK,
        UNKNOWN_ERROR,
        OVER_QUERY_LIMIT,
        REQUEST_DENIED,
        INVALID_REQUEST,
        ZERO_RESULTS
    };

    protected Response(JSONObject rawResponse) throws JSONException {
        htmlAttributions = rawResponse.getString("html_attributions");
        status = Status.valueOf(rawResponse.getString("status"));
    }

    public String getHtmlAttributions() {
        return htmlAttributions;
    }

    public Status getStatus() {
        return status;
    }
}
